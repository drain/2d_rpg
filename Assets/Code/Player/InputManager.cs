using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public List<InputSource> inputSources = new List<InputSource>();

    public InputSource GetInputSourceForAction(InputAction ia)
    {
        for (int i = 0; i < inputSources.Count; i++)
            if (inputSources[i].action == ia)
                return inputSources[i];
        return null;
    }
}

[System.Serializable]
public class InputSource
{
    public KeyCode pc_key;
    public UnityEngine.UI.Button button;
    public InputAction action;
}

public enum InputAction
{
    MoveLeft,
    MoveRight,
    MoveUp,
    MoveDown,
    Interact,
    Attack,
    UseQuickSlot1,
    UseQuickSlot2,
    UseQuickSlot3,
    UseQuickSlot4,
    UseQuickSlot5,
    UseQuickSlot6,
    OpenMenu,
    OpenInventory,
    OpenProfessionUI,
    SaveGame
}
