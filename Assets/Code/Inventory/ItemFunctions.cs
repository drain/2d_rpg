using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemFunctions : MonoBehaviour
{

    public delegate void ItemDelegate(params object[] args);
    public Dictionary<string, ItemDelegate> itemFunctions = new Dictionary<string, ItemDelegate>();

    void Awake()
    {
        InitDictionary();
    }

    public void InitDictionary()
    {
        itemFunctions.Add("Heal", delegate (object[] args) {

            Slot slot = (Slot)args[0];
            Stats stats = (Stats)args[1];
            float heal = (float)args[2];

            if (stats.health < stats.maxHealth)
            {
                stats.Heal(heal);
                slot.currentStack--;
                slot.UpdateStackText();
            }
        });
    }
}