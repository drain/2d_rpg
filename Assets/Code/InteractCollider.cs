using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractCollider : MonoBehaviour
{
    public InteractType interactType;

    [SerializeField]
    private Item m_lootItem;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "PlayerObject")
        {
            PromptInteraction(interactType, true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerObject")
        {
            PromptInteraction(interactType, false);
            GameManager.Instance.PlayerManager.ShowPromptText("", false);
        }
    }

    private void PromptInteraction(InteractType type, bool status)
    {
        switch(type)
        {
            case InteractType.Fish:
                //TODO: make this work from profession manager better
                Fishing fishing = (Fishing)GameManager.Instance.PlayerManager.professionManager.GetProfession(ProfessionType.Fishing);
                fishing.allowFishing = status;
                GameManager.Instance.PlayerManager.ShowPromptText("Press F to fish!", true);
                break;
            case InteractType.LootItem:
                //"Press {interactkey} to loot {item}!"
                //GameManager.Instance.PlayerManager.ShowPromptText("Press {} to loot item", true);
                if (status)
                    GameManager.Instance.PlayerManager.playerInventory.AddItem(m_lootItem);
                Destroy(gameObject);
                break;
        }
    }

    public void SetLootItem(Item item)
    {
        m_lootItem = item;
        //change this to spriteingame when we have the graphics
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if(sr != null)
        {
            //change the sprite but keep the original size
            sr.drawMode = SpriteDrawMode.Sliced;
            Vector2 originalSize = sr.size;
            sr.sprite = m_lootItem.icon;
            sr.size = originalSize;
            sr.drawMode = SpriteDrawMode.Simple;
        }
            
    }

}

public enum InteractType
{
    Talk,
    OpenChest,
    LootItem,
    Fish,
    CutWood,
    Mine,
    Skin
}
