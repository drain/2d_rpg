using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public List<Destroyable> destroyablesInLevel = new List<Destroyable>();
    public List<NPC> npcsInScene = new List<NPC>();

    private PlayerEntity m_playerEntity;

    /// <summary>
    /// returns sqrMagnitude between e1 and e2
    /// </summary>
    /// <param name="e1"></param>
    /// <param name="e2"></param>
    /// <returns></returns>
    public float GetDistanceBetweenEntities(Entity e1, Entity e2)
    {
        return (e1.transform.position - e2.transform.position).sqrMagnitude;
    }

    private void Start()
    {
        destroyablesInLevel.AddRange(FindObjectsOfType<Destroyable>());
        npcsInScene.AddRange(FindObjectsOfType<NPC>());

        m_playerEntity = FindObjectOfType<PlayerEntity>();
    }


    private void FixedUpdate()
    {
        //TODO: maybe not loop it as often?
        for(int i = npcsInScene.Count - 1; i >= 0; i--)
        {
            if(npcsInScene[i] == null)
            {
                npcsInScene.RemoveAt(i);
                continue;
            }

            if(npcsInScene[i].IsHostile() && npcsInScene[i].IsFacingTarget())
            {
                //if close enough -> attack
                if (npcsInScene[i].IsCloseEnoughToMelee(m_playerEntity.transform.position))
                {
                    npcsInScene[i].Stop();
                    npcsInScene[i].PerformLightAttack();
                }
                else
                    //move towards player
                    npcsInScene[i].MoveTowards(m_playerEntity.transform.position);

                npcsInScene[i].UpdateMeleeTimer();
            }
            else
            {
                npcsInScene[i].Idle();
            }
        }
    }
}
