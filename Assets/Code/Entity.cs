using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Stats))]
public class Entity : MonoBehaviour
{
    public int entityID;
    public string entityName;

    public bool isEnabled = true;

    private Stats m_stats = null;

    public Stats Stats { get { return m_stats; } }

    public bool canDie = false;


    protected virtual void Awake()
    {
        if (GetComponent<Stats>())
            m_stats = GetComponent<Stats>();
    }

    public virtual void Died()
    {
        
    }

}
