using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    private ItemFunctions m_itemFunctions;
    public List<string> parameters;

    //this is used to add the item values in inspector
    public List<Item> itemConstructionList = new List<Item>();
    //then they are accessed from this static dictionary
    public static Dictionary<int, Item> items = new Dictionary<int, Item>();

    //this is used to add the item values in inspector
    public List<LootTable> lootTablesConstructionList = new List<LootTable>();
    //then they are accessed from this static dictionary
    public static Dictionary<int, LootTable> lootTables = new Dictionary<int, LootTable>();

    private void Awake()
    {
        m_itemFunctions = GetComponent<ItemFunctions>();
        InitItems();

        InitializeLootTables();
    }

    private void InitItems()
    {
        //populate the dictionary of items
        for(int i = 0; i < itemConstructionList.Count; i++)
        {
            items.Add(itemConstructionList[i].itemID, itemConstructionList[i]);
        }

        //construct loot table dictionary (enemyID, loottable)
        for (int i = 0; i < lootTablesConstructionList.Count; i++)
        {
            lootTables.Add(lootTablesConstructionList[i].ownerID, lootTablesConstructionList[i]);
        }
    }

    public void UseItem(Slot slot)
    {
        m_itemFunctions.itemFunctions.TryGetValue(slot.item.itemFunction, out ItemFunctions.ItemDelegate itemDelegate);

        if (itemDelegate != null)
        {
            switch (slot.item.itemFunction)
            {
                case "Heal":
                    Stats stats = GameManager.Instance.PlayerManager.playerObject.GetComponent<Stats>(); //slot.item.owner.GetComponent<Stats>();
                    float upcomingHeal = (slot.item.parameters[0].getFloatValue() / 100) * stats.maxHealth;

                    itemDelegate(slot, stats, upcomingHeal);
                    break;
            }
        }
    }

    /// <summary>
    /// returns first item found with same itemname
    /// (from start -> end)
    /// </summary>
    /// <param name="itemName"></param>
    /// <returns></returns>
    public static Item GetItemByName(string itemName)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].itemName == itemName)
            {
                return items[i];
            }
        }
        return null;
    }

    public static Item GetItemById(int id)
    {
        //return regardless of being null or not
        items.TryGetValue(id, out Item item);
        return item;
    }

    private void InitializeLootTables()
    {
        foreach(int id in lootTables.Keys)
        {
            if(lootTables[id] != null)
                lootTables[id].GenerateThresholds();
        }
    }

    [System.Serializable]
    public class LootTable
    {
        public int ownerID;
        public List<LootDrop> table = new List<LootDrop>();
        private int maximumPropability;
        //public List<Item> questItems = new List<Item>();

        public Item PickItem(int itemID)
        {
            for(int i = 0; i < table.Count;  i++)
            {
                if (table[i].itemID == itemID)
                    return GetItemById(table[i].itemID);
            }

            return null;
        }

        public Item GetRandomItem()
        {
            //roll between 0-maxpropability -> give item accordingly where the roll landed
            int rnd = Random.Range(0, maximumPropability + 1);

            // why this? is it because float? not sure
            if (rnd == maximumPropability)
                rnd -= 1;

            //TODO: write this as for loop to make it more readable
            //loop until we get the item floor (rnd)
            int index = 0;
            while (table[index].dropChance <= rnd)
            {
                index++;
            }

            return GetItemById(table[index].itemID);
        }

        public void GenerateThresholds()
        {
            //add all % drop changes together
            for (int i = 0; i < table.Count; i++)
            {
                //dont add first since we add this + previous to make the next floor
                if (i != 0)
                {
                    table[i].dropChance += table[i - 1].dropChance;
                }
            }

            //total floor is from the last
            maximumPropability = table[table.Count - 1].dropChance;

            //loop through again to calculate the drop % threshold from the maximum value
            for (int i = 0; i < table.Count; i++)
            {
                //get the % value
                //need to calculate in float then convert back to int (maybe there is other way)
                float temp = ((float)maximumPropability / 100) * table[i].dropChance;
                table[i].dropChance = System.Convert.ToInt32(temp);
            }

            //set the maximum to the new max value
            maximumPropability = table[table.Count - 1].dropChance;
        }

    }

    [System.Serializable]
    public class LootDrop
    {
        public int itemID;
        public int dropChance;
    }
}
 
 
 
 