using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[System.Serializable]
public class Slot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    public enum SlotType
    {
        Equip,
        Inventory,
        CraftingInput,
        CraftingOutput
    };

    public SlotType slotType;
    public Item item;
    public bool hasItem = false;
    public int currentStack;
    public int maxStack;
    public Inventory.EquipSlot equipSlot;

    private Transform m_transform;
    private Inventory m_inventory;
    private Image m_image;
    private Text m_stackText;

    public Transform setTooltipItem;

    //TODO: this is kinda sus
    public static Transform ItemToolTip;

    private ItemManager m_itemManager;

    public void SetItem(Item itemToAdd)
    {
        if (itemToAdd != null)
        {
            item = itemToAdd;
            hasItem = true;
            if (itemToAdd.isStackable)
            {
                currentStack = itemToAdd.currentStack;
                maxStack = item.maxStack;
                UpdateStackText();
            }
            SetImgToSlot(itemToAdd.icon);
        }
    }
    public void EmptySlot()
    {
        hasItem = false;
        item = null;
        currentStack = 0;
        maxStack = 0;
        m_image.sprite = null;

        if (slotType != SlotType.Equip)
        {
            if(m_stackText == null)
                m_stackText = m_transform.GetChild(1).GetComponent<Text>();
            m_stackText.text = "";
        }
    }

    #region IDropHandler implementation

    void IDropHandler.OnDrop(PointerEventData eventData)
    {
        m_inventory.MoveItem(DragHandler.draggedItem, DragHandler.draggedSlot, this);
    }

    #endregion

    #region IPointerEnterHandler implementation

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        if (item != null && hasItem)
        {
            ShowTooltip(item);
            ItemToolTip.gameObject.SetActive(true);
            ItemToolTip.position = new Vector2(m_transform.position.x, m_transform.position.y);
        }
    }

    #endregion

    #region IPointerExitHandler implementation

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        ItemToolTip.gameObject.SetActive(false);
        ClearTooltip();
    }

    #endregion

    #region IPointerClickHandler implementation

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right && item != null && hasItem && item.type == Item.Type.other)
            m_itemManager.UseItem(this);
        else if (eventData.button == PointerEventData.InputButton.Middle && item != null && hasItem && slotType == SlotType.Inventory)
            m_inventory.RemoveItemFromInventory(m_inventory.inventorySlots.IndexOf(this));

        //TODO: maybe add menu that opens when this is clicked with actions
        // or just equip the item or something on left click

    }

    #endregion

    void Awake()
    {
        m_transform = transform;
        InitSlot();
        m_itemManager = FindObjectOfType<ItemManager>();
        if (setTooltipItem != null)
            ItemToolTip = setTooltipItem;
    }

    void InitSlot()
    {
        item = null;
        m_image = m_transform.GetChild(0).GetComponent<Image>();
        if (slotType != SlotType.Equip)
            m_stackText = m_transform.GetChild(1).GetComponent<Text>();
        m_inventory = FindObjectOfType<Inventory>();
    }

    public void SetImgToSlot(Sprite img)
    {
        if (img != null && m_image != null)
            m_image.sprite = img;
    }

    public void ShowTooltip(Item item)
    {
        m_inventory.tooltipText.text = item.GetTooltip();
    }

    public void ClearTooltip()
    {
        m_inventory.tooltipText.text = "";
    }

    public void UpdateStackText()
    {
        if (item != null && item.isStackable && hasItem)
        {
            if(m_stackText == null)
                m_stackText = m_transform.GetChild(1).GetComponent<Text>();

            m_stackText.text = currentStack + "/" + maxStack;
            item.currentStack = currentStack;

            if (item.isStackable && currentStack == 0)
            {
                EmptySlot();
            }
        }
    }
}