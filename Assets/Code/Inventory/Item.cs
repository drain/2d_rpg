using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item", order = 1)]
public class Item : ScriptableObject
{
    public string itemName = "";
    public int itemID = 0;
    public int lvlReq = 0;
    public string description;
    public Sprite icon;

    public string useEffect = "";
    public string itemFunction = "";
    public List<ItemFunctionParameter> parameters;
    public float useCooldownSeconds = 0;

    public string lore;

    public Sprite spriteInGame;
    public bool isStackable;

    [HideInInspector]
    public GameObject pickUp;

    public Type type;
    public Rarity rarity;

    public int maxStack;
    public int currentStack;

    public bool hasAnim;
    public Anim[] frames;

    [System.Serializable]
    public class ItemFunctionParameter
    {
        public string value;

        public int getIntValue()
        {
            return System.Convert.ToInt32(value);
        }

        public float getFloatValue()
        {
            return System.Convert.ToSingle(value);
        }

        public string getStringValue()
        {
            return value;
        }

    }

    [System.Serializable]
    public class Anim
    {
        public Sprite[] animPieces;
    }

    public enum Type
    {
        equipment,
        other
    };
    public enum Rarity
    {
        common,
        uncommon,
        rare,
        epic
    };
    

    public Item()
    {

    }

    public Item(Item item)
    {
        DeepCopy(item);
    }

    //https://stackoverflow.com/questions/1573453/i-need-to-implement-c-sharp-deep-copy-constructors-with-inheritance-what-patter
    //to preserve the class hierarchy when making copies
    public virtual object Clone()
    {
        return Instantiate(this);
    }

    public virtual void InitItem()
    {

    }

    public virtual string GetTooltip()
    {
        string returnString = "";
        string splDmgs = "";
        string splResis = "";
        string color = "white";

        switch (rarity)
        {
            case Rarity.common:
                color = "white";
                break;
            case Rarity.uncommon:
                color = "green";
                break;
            case Rarity.rare:
                color = "blue";
                break;
            case Rarity.epic:
                color = "magenta";
                break;
        }

        string tempUseEffect = "";
        if (useEffect != "")
        {
            tempUseEffect += useEffect;
        }

        returnString += "<color=" + color + ">" + itemName + "</Color> \n" + "requires lvl " + lvlReq + "\n" + type.ToString() + " \n<color=green>" + tempUseEffect + "</color>\n";

        return returnString;
    }

    public virtual void DeepCopy(Item itemToCopy)
    {
        itemName = itemToCopy.itemName;
        itemID = itemToCopy.itemID;
        lvlReq = itemToCopy.lvlReq;
        description = itemToCopy.description;
        icon = itemToCopy.icon;

        lore = itemToCopy.lore;

        spriteInGame = itemToCopy.spriteInGame;
        isStackable = itemToCopy.isStackable;

        pickUp = itemToCopy.pickUp;

        useCooldownSeconds = itemToCopy.useCooldownSeconds;
        useEffect = itemToCopy.useEffect;
        itemFunction = itemToCopy.itemFunction;

        List<ItemFunctionParameter> m_parameters = new List<ItemFunctionParameter>();
        foreach (ItemFunctionParameter parameter in itemToCopy.parameters)
        {
            m_parameters.Add(parameter);
        }
        parameters = m_parameters;

        type = itemToCopy.type;
        rarity = itemToCopy.rarity;

        maxStack = itemToCopy.maxStack;
        currentStack = itemToCopy.currentStack;

        hasAnim = itemToCopy.hasAnim;
        frames = itemToCopy.frames;
    }
}
