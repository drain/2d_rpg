using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfessionManager : MonoBehaviour
{
    public List<Profession> professionsConstructionList = new List<Profession>();
    public Dictionary<ProfessionType, Profession> professions = new Dictionary<ProfessionType, Profession>();

    //TODO: Move these to PlayerUI class and get them to here from that class
    public Slot slotPrefab;
    public GameObject professionUIMain;
    public GameObject professionUISub;

    public VerticalLayoutGroup professionList;
    public Button professionSelectButton;
    public Button backButton;
    public Text professionSelectedText;
    public Dropdown recipeSelect;
    public Text recipeInfoText;
    public HorizontalLayoutGroup inputSlots;
    public HorizontalLayoutGroup outputSlots;
    public Button craftButton;
    public GameObject craftingArea;
    public InputField craftingAmount;

    private Dictionary<ProfessionType, Button> m_professionSelectButtons = new Dictionary<ProfessionType, Button>();
    private ProfessionType m_currentOpenProfessionType;

    private void Start()
    {
        foreach (Profession profession in professionsConstructionList)
        {
            Profession p = Instantiate(profession);
            professions.Add(profession.professionType, p);
        }

        InitProfessionUI();
    }

    private void InitProfessionUI()
    {
        foreach (Profession p in professionsConstructionList)
        {
            GameObject button = Instantiate(professionSelectButton.gameObject, professionList.transform);
            Button b = button.GetComponent<Button>();
            b.onClick.AddListener(() =>
            {
                SelectProfession(p.professionType);
            });
            b.GetComponentInChildren<Text>().text = p.professionType.ToString() + " " + p.currentSkill + "/" + p.maxSkill;
            m_professionSelectButtons.Add(p.professionType, b);
        }

        backButton.onClick.AddListener(() =>
        {
            professionUISub.SetActive(false);
        });
    }

    public void UpdateProfessionSkillLevelInUI(Profession p)
    {
        //update selected profession button
        if(p.professionType == m_currentOpenProfessionType)
            professionSelectedText.text = p.professionType.ToString() + " " + p.currentSkill + "/" + p.maxSkill;

        //update main profession select buttons
        m_professionSelectButtons.TryGetValue(p.professionType, out Button b);
        if(b != null)
        {
            b.GetComponentInChildren<Text>().text = p.professionType.ToString() + " " + p.currentSkill + "/" + p.maxSkill;
        }
    }

    public void SelectProfession(ProfessionType pType)
    {
        //get the right profession
        //populate info in sub UI
        //hide main UI (or spawn infront)

        Profession p = GetProfession(pType);
        //todo: add profession icon
        professionSelectedText.text = p.professionType.ToString() + " " + p.currentSkill + "/" + p.maxSkill;
        m_currentOpenProfessionType = p.professionType;

        recipeSelect.options.Clear();
        Dropdown.OptionData baseData = new Dropdown.OptionData("About");
        recipeSelect.options.Add(baseData);

        foreach (Recipe r in p.recipeList)
        {
            Dropdown.OptionData optionData = new Dropdown.OptionData(r.name);
            recipeSelect.options.Add(optionData);
        }

        recipeSelect.onValueChanged.RemoveAllListeners();
        recipeSelect.value = 0;
        recipeSelect.onValueChanged.AddListener((value) =>
        {
            if (value > 0)
                SelectRecipe(p.GetRecipeByIndex(value - 1));
            else
                SelectProfession(pType);
        });

        recipeInfoText.text = p.desc;

        craftingArea.SetActive(false);
        craftButton.gameObject.SetActive(false);
        professionUISub.SetActive(true);
    }

    private void ClearCraftingArea()
    {
        foreach (Transform child in inputSlots.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in outputSlots.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void SelectRecipe(Recipe r)
    {
        ClearCraftingArea();

        List<Slot> inSlots = new List<Slot>();
        string materialString = "";

        foreach (Recipe.CraftingMaterial cm in r.requiredMaterials)
        {
            GameObject go = Instantiate(slotPrefab.gameObject, inputSlots.transform);
            Slot s = go.GetComponent<Slot>();
            s.slotType = Slot.SlotType.CraftingInput;
            if (cm.amount > 1)
            {
                s.maxStack = cm.amount;
            }
            s.GetComponent<RectTransform>().sizeDelta = new Vector2(120, 120);
            inSlots.Add(s);
            materialString += "\n" + cm.amount + "x " + cm.item.itemName;
        }

        GameObject output = Instantiate(slotPrefab.gameObject, outputSlots.transform);
        Slot outSlot = output.GetComponent<Slot>();
        outSlot.GetComponent<RectTransform>().sizeDelta = new Vector2(120, 120);
        outSlot.slotType = Slot.SlotType.CraftingOutput;
        Recipe recipe = r;

        craftButton.onClick.RemoveAllListeners();
        craftButton.onClick.AddListener(() =>
        {
            CraftItem(recipe, inSlots.ToArray(), outSlot);

        });

        recipeInfoText.text = r.output.item.description + "\n\n" + r.output.item.itemName + "\n" + r.output.item.useEffect + "\n\n Materials" + materialString;

        craftingArea.SetActive(true);
        craftButton.gameObject.SetActive(true);
    }

    public void CraftItem(Recipe recipe, Slot[] inputs, Slot output)
    {
        //validate this
        if (craftingAmount.text == "")
            return;

        int cAmount = System.Convert.ToInt32(craftingAmount.text);
        if (cAmount <= 0)
            cAmount = 1;

        //we can only craft maximum of 1 max stack per time
        if ((recipe.output.amount * cAmount) > recipe.output.item.maxStack)
            return;

        int req = recipe.requiredMaterials.Count;
        int have = 0;

        foreach(Recipe.CraftingMaterial required in recipe.requiredMaterials)
        {
            foreach (Slot input in inputs)
            {
                if (input.item != null && required.item.itemID == input.item.itemID && input.item.currentStack >= (required.amount * cAmount))
                {
                    have++;
                    input.item.currentStack -= (required.amount * cAmount);
                }
            }  
        }

        if(have == req)
        {
            foreach (Slot input in inputs)
            {
                if (input.item.currentStack > 0)
                {
                    GameManager.Instance.PlayerManager.playerInventory.AddItem(input.item);
                    input.EmptySlot();
                }
                else
                    input.EmptySlot();
            }

            Item outputItem = (Item)recipe.output.item.Clone();
            outputItem.currentStack *= recipe.output.amount * cAmount;

            output.SetItem(outputItem);

            craftingAmount.text = "";

            //add some exp for crafting
            GameManager.Instance.PlayerManager.playerObject.GetComponent<Stats>().AddExp(recipe.expYield*cAmount);

            //TODO: multiple base yields + somekind of decay system so low recipes dont grant exp infinitely
            GetProfession(recipe.professionType).IncreaseSkill(1*cAmount);
            GameManager.Instance.PlayerManager.playerUI.DisplayInfoText("SKill in " + recipe.professionType.ToString() + " increased to " + GetProfession(recipe.professionType).currentSkill + "!");
            GameManager.Instance.PlayerManager.playerUI.DisplayInfoText($"Made: {output.item.currentStack}x {output.item.itemName}");
            GameManager.Instance.PlayerManager.playerUI.RefreshPlayerInfoText();
        }
        else
        {
            Debug.Log("Missing materials");
        }


    }

    public Profession GetProfession(ProfessionType pType)
    {
        if(professions.TryGetValue(pType, out Profession profession))
            return profession;
        else
            return null;
    }
}

[System.Serializable]
public class Profession : ScriptableObject
{
    public ProfessionType professionType;
    public string desc;
    public int currentSkill;
    public int maxSkill = 150;
    public List<Recipe> recipeList = new List<Recipe>();

    public Recipe GetRecipeByIndex(int index)
    {
        return recipeList[index];
    }

    public void IncreaseSkill(int amount)
    {
        currentSkill += amount;
        if (currentSkill >= maxSkill)
            currentSkill = maxSkill;

        //TODO: update skill lvl in every text
        ProfessionManager professionManager = GameManager.Instance.PlayerManager.professionManager;
        professionManager.UpdateProfessionSkillLevelInUI(this);
    }
}

[System.Serializable]
public enum ProfessionType
{
    Fishing,
    Cooking,
    Mining,
    Woodcutting,
    Herbalism,
	TreasureFinding,
    Skinning,
	ArmorCrafting,
	WeaponCrafting,
	Alchemy,
	Building,
	BoatCrafting
	
}

[System.Serializable]
public class Recipe
{
    public string name;
    public bool learned;
    public List<CraftingMaterial> requiredMaterials = new List<CraftingMaterial>();
    public CraftingMaterial output;
    public int expYield;
    public ProfessionType professionType;

    [System.Serializable]
    public class CraftingMaterial
    {
        public Item item;
        public int amount;
    }
}
