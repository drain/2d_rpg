using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    public Canvas uiCanvas;
    public float infoMessageTimeInSeconds;

    [SerializeField]
    private GameObject m_informationTextComponent;
    [SerializeField]
    private Text m_informationText;
    [SerializeField]
    private Text m_playerInfoText;

    private Queue<string> m_infoMessageQueu = new Queue<string>();
    private bool m_isAlreadyDisplaying = false;

    public void DisplayInfoText(string textToDisplay)
    {
        if (m_informationText != null && !m_isAlreadyDisplaying)
        {
            m_informationText.text = textToDisplay;
            StartCoroutine(DisplayInfoTextTimer(infoMessageTimeInSeconds));
        }
        else if(m_informationText != null)
            m_infoMessageQueu.Enqueue(textToDisplay);
    }

    private void Update()
    {
        //TODO: do this through inputmanager
        if (Input.GetKeyDown(KeyCode.Escape))
            m_infoMessageQueu.Clear();

        //TODO: make this update less often than normal update
        if (m_infoMessageQueu.Count > 0 && !m_isAlreadyDisplaying)
        {
            DisplayInfoText(m_infoMessageQueu.Dequeue());
        }
    }

    private IEnumerator DisplayInfoTextTimer(float timeInSeconds)
    {
        m_isAlreadyDisplaying = true;
        m_informationTextComponent.SetActive(true);
        yield return new WaitForSeconds(timeInSeconds);
        m_informationTextComponent.SetActive(false);
        m_informationText.text = "";
        m_isAlreadyDisplaying = false;
    }


    public void RefreshPlayerInfoText()
    {
        Stats stats = GameManager.Instance.PlayerManager.playerObject.GetComponent<Stats>();
        m_playerInfoText.text = "Lvl: " + stats.exp.GetCurrentLvl().ToString() + "\n" +
                                "Hp: " + stats.health + "/" + stats.maxHealth + "\n" +
                                "Mana: " + 50 + "/" + 50 + "\n" +
                                "Exp: " + stats.exp.currentExp + "/" + stats.exp.expToLvl.ToString();
    }
}
