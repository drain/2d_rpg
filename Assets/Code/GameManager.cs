using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public InputManager InputManager;
    public PlayerManager PlayerManager;



    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        InputManager = FindObjectOfType<InputManager>();
        PlayerManager = FindObjectOfType<PlayerManager>();
    }
}
