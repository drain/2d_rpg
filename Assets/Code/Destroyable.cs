using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : Entity, IDropsLoot
{
    public bool dropsLoot = false;

    public int maxItemDropAmount;

    public InteractCollider lootPrefab;

    protected override void Awake()
    {
        base.Awake();
    }

    public override void Died()
    {
        DropLoot();
        Destroy(this.gameObject);
    }

    public void DropLoot()
    {
        if (dropsLoot)
        {
            for (int i = 0; i < maxItemDropAmount; i++)
            {
                if (lootPrefab != null)
                {
                    ItemManager.lootTables.TryGetValue(entityID, out ItemManager.LootTable lootTable);
                    if (lootTable == null)
                        return;

                    Item item = lootTable.GetRandomItem();
                    
                    if (item != null)
                    {
                        InteractCollider lootObject = Instantiate(lootPrefab, transform.position, Quaternion.identity);
                        lootObject.SetLootItem(item);
                        Debug.Log(entityName + " dropped item " + item.itemName);
                    }
                    else
                        Debug.Log(entityName + " dropped nothing :(");
                }
            }
        }
    }
}
