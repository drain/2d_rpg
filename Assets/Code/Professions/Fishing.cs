using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishing : Profession
{
    public Item testFish;
    public bool allowFishing;

    private void Start()
    {
        professionType = ProfessionType.Fishing;
        currentSkill = 0;
        maxSkill = 150;
    }

    public void StartFishing()
    {
        //todo: check if facing fishable tile
        if(Random.Range(1,101) > 25)
        {
            GameManager.Instance.PlayerManager.playerUI.DisplayInfoText("Caught a fish: " + testFish.name + "!");
            if(currentSkill < 150 && Random.Range(1,101) > 40)
            {
                IncreaseSkill(1);
                GameManager.Instance.PlayerManager.playerUI.DisplayInfoText("Skill in fishing increased to " + currentSkill + "!");
            }

            
            GameManager.Instance.PlayerManager.playerInventory.AddItem(testFish);   
        }
    }
}
