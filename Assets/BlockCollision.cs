using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCollision : MonoBehaviour
{
    private void Start()
    {
        Collider2D main = GetComponent<Collider2D>();
        Collider2D block = transform.GetChild(0).GetComponent<Collider2D>();

        if(main != null && block != null)
        {
            Physics2D.IgnoreCollision(main, block, true);
        }
    }

}
