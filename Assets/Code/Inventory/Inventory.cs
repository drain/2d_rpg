using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class Inventory : MonoBehaviour
{
    //public List<InventoryItem> itemsInInventory = new List<InventoryItem>();
    public SpriteRenderer hat, weaponSlot;

    public Vector2 mousePos;
    public enum EquipSlot
    {
        none,
        head,
        gloves,
        chest,
        boots,
        neck,
        cape,
        ring1,
        ring2,
        trinket,
        weapon,
    };

    public List<Slot> equippedItems = new List<Slot>();
    public List<Slot> inventorySlots = new List<Slot>();
    public Text tooltipText;
    public Text statsText;
    private Stats m_playerStats;

    public GameObject inventoryUI;

    void Awake()
    {
        InitInventory();  
    }

    void Start()
    {
        m_playerStats = GameManager.Instance.PlayerManager.playerObject.GetComponent<Stats>();
        UpdateStatText();
    }

    void InitInventory()
    {
        //to preserve the order of the inventory slots, im lazy and cba to drag them in inspector, sort by index in hierarchy
        Slot[] slots = FindObjectsOfType<Slot>().OrderBy(m => m.transform.GetSiblingIndex()).ToArray();
        foreach (Slot slot in slots)
        {
            if (slot.slotType == Slot.SlotType.Equip)
            {
                equippedItems.Add(slot);
            }
            else if (slot.slotType == Slot.SlotType.Inventory)
            {
                inventorySlots.Add(slot);
            }
        }
    }

    public void SetSprite(Equipment equipment)
    {
        switch (equipment.equipSlot)
        {
            case EquipSlot.head:
                /*if (item.hasAnim)
                {
                    LegacySpriteAnim anim = hat.GetComponent<LegacySpriteAnim>();
                    anim.sprites = item.frames[0].animPieces;
                    anim.enabled = true;
                }*/
                if(hat != null)
                    hat.sprite = equipment.spriteInGame;
                break;
            case EquipSlot.chest:
                if (equipment.hasAnim)
                {
                    for (int i = 0; i < equipment.frames.Length; i++)
                    {
                        /*LegacySpriteAnim anim = robe[i].piece.GetComponent<LegacySpriteAnim>();
                        if (anim != null)
                        {
                            anim.sprites = item.frames[i].animPieces;
                            anim.enabled = true;
                        }*/
                    }
                }
                /*for (int i = 0; i < robe.Length; i++)
                {
                    for (int j = 0; j < equipment.robe.pieceName.Length; j++)
                    {
                        if (string.Equals(robe[i].pieceName, equipment.robe.pieceName[j], System.StringComparison.OrdinalIgnoreCase))
                        {
                            robe[i].piece.sprite = equipment.robe.piece[j];
                        }
                    }
                }*/
                break;
            case EquipSlot.weapon:
                if (equipment.hasAnim)
                {
                    /*LegacySpriteAnim anim = weaponSlot.GetComponent<LegacySpriteAnim>();
                    anim.sprites = item.frames[0].animPieces;
                    anim.enabled = true;*/
                }
                if(weaponSlot != null)
                    weaponSlot.sprite = equipment.spriteInGame;
                break;
        }
    }

    public void RemoveSprite(Equipment equipment)
    {
        switch (equipment.equipSlot)
        {
            case EquipSlot.head:
                if (equipment.hasAnim)
                {
                    /*LegacySpriteAnim anim = hat.GetComponent<LegacySpriteAnim>();
                    anim.sprites = new Sprite[0];
                    anim.enabled = false;*/
                }
                if(hat != null)
                    hat.sprite = null;
                break;
            case EquipSlot.chest:
                //set basic sprites back
                if (equipment.hasAnim)
                {
                    for (int i = 0; i < equipment.frames.Length; i++)
                    {
                        /*LegacySpriteAnim anim = robe[i].piece.GetComponent<LegacySpriteAnim>();
                        if (anim != null)
                        {
                            anim.sprites = new Sprite[0];
                            anim.enabled = false;
                        }*/
                    }
                }
                /*for (int i = 0; i < robe.Length; i++)
                {
                    if(robe[i].piece != null)
                        robe[i].piece.sprite = defaultRobe[i];
                }*/
                break;
            case EquipSlot.weapon:
                if (equipment.hasAnim)
                {
                    /*LegacySpriteAnim anim = weaponSlot.GetComponent<LegacySpriteAnim>();
                    anim.sprites = new Sprite[0];
                    anim.enabled = false;*/
                }
                if(weaponSlot != null)
                    weaponSlot.sprite = null;
                break;
        }
    }

    public bool AddItem(Item item)
    {
        if (item != null)
        {
            Item itemCopy = (Item)item.Clone();

            if (itemCopy.isStackable)
            {
                //check if we have these same items in inventory
                foreach (Slot slot in inventorySlots)
                {
                    if (slot.item != null && slot.item.itemName == itemCopy.itemName && slot.hasItem)
                    {
                        if (slot.currentStack < slot.maxStack)
                        {
                            //add stacks to the found item stack
                            slot.currentStack += itemCopy.currentStack;

                            if (slot.currentStack > slot.maxStack)
                            {
                                //save ones leftover to the itemcopy and keep looping
                                int over = slot.currentStack - slot.maxStack;
                                itemCopy.currentStack = over;
                                slot.currentStack = slot.maxStack;
                            }
                            else
                                itemCopy.currentStack = 0;

                            slot.UpdateStackText();

                            //we used all the stacks so we succesfully added the item
                            if (itemCopy.currentStack <= 0)
                                return true;
                        }
                    }
                }
            }

            //we didnt find any of these items so we need to add it to the first empty slot
            foreach (Slot slot in inventorySlots)
            {
                if (!slot.hasItem)
                {
                    slot.SetItem(itemCopy);
                    return true;
                }
            }
        }

        //we didnt find any stacks or empty slots in inventory -> inventory is full and we cannot pickup this fully
        return false;
    }

    public void RemoveItemFromInventory(int index)
    {
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            if (i == index)
            {
                inventorySlots[i].EmptySlot();
            }
        }
    }

    //TODO: Optimize this and break it into smaller functions... this is way too long
    public void MoveItem(Item item, Slot moveFrom, Slot moveTo)
    {
        if (item != null)
        {
            //move inside inventory
            if (moveFrom.slotType == Slot.SlotType.Inventory && moveTo.slotType == Slot.SlotType.Inventory)
            {
                if (!moveTo.hasItem && item != null)
                {
                    moveTo.SetItem(item);
                    moveFrom.EmptySlot();
                }
                //swap items
                else
                {
                    //TODO: check for stacks here and split the stacks correctly
                    //if same item and its stackable
                    if(moveTo.item.isStackable && moveTo.item.itemID == moveFrom.item.itemID)
                    {
                        //split stacks
                        int maxStackAmount = moveTo.item.maxStack;
                        int totalAmount = moveTo.item.currentStack + moveFrom.item.currentStack;

                        if (totalAmount > maxStackAmount)
                        {
                            //TODO: make it swap smaller stack with the place of the full stack if we want.. kinda useless
                            moveTo.currentStack = maxStackAmount;
                            moveFrom.currentStack = (totalAmount - maxStackAmount);
                            moveTo.UpdateStackText();
                            moveFrom.UpdateStackText();
                        }
                        else
                        {
                            moveTo.currentStack = totalAmount;
                            moveTo.UpdateStackText();
                            moveFrom.EmptySlot();
                        }

                    }
                    //swap items
                    else
                    {
                        Item temp = moveTo.item;
                        Item temp2 = moveFrom.item;
                        moveTo.EmptySlot();
                        moveFrom.EmptySlot();
                        moveTo.SetItem(temp2);
                        moveFrom.SetItem(temp);
                    }
                }
            }
            //move from equipped slot to inventory
            if (moveFrom.slotType == Slot.SlotType.Equip && moveTo.slotType == Slot.SlotType.Inventory)
            {
                if (!moveTo.hasItem)
                {
                    m_playerStats.RemoveItemStats((Equipment)item);
                    UpdateStatText();
                    moveTo.SetItem(item);
                    RemoveSprite((Equipment)item);
                    moveFrom.EmptySlot();

                    //show slot text
                    moveFrom.transform.GetChild(2).gameObject.SetActive(true);
                }
                else
                {
                    //check if we are swapping item with one in inventory and we have high enought level to use it
                    if(item.type == Item.Type.equipment && moveTo.item.type == Item.Type.equipment)
                    {
                        Equipment moveToEquipment = (Equipment)moveTo.item;
                        Equipment movedEquipment = (Equipment)item;

                        //same equipslot (gloves, helm, chest, etc)
                        if(moveToEquipment.equipSlot == movedEquipment.equipSlot)
                        {
                            //we can use the new item?
                            if (moveTo.item.lvlReq <= m_playerStats.exp.GetCurrentLvl())
                            {
                                Item temp = moveTo.item;
                                Item temp2 = moveFrom.item;
                                if (EquipItem(moveToEquipment))
                                {
                                    Equipment equipment = (Equipment)item;
                                    m_playerStats.RemoveItemStats(equipment);
                                    m_playerStats.AddItemStats(equipment);

                                    moveTo.EmptySlot();
                                    moveFrom.EmptySlot();

                                    RemoveSprite((Equipment)temp2);
                                    SetSprite((Equipment)temp);

                                    moveTo.SetItem(temp2);
                                    moveFrom.SetItem(temp);
                                    UpdateStatText();
                                }
                            }
                        }
                    }
                }
            }
            //equip item
            if (moveFrom.slotType == Slot.SlotType.Inventory && moveTo.slotType == Slot.SlotType.Equip &&
                moveFrom.item.type == Item.Type.equipment)
            {
                if (!moveTo.hasItem)
                {
                    Equipment moveFromEquipment = (Equipment)moveFrom.item;
                    if (moveTo.equipSlot == moveFromEquipment.equipSlot)
                    {
                        Equipment equipment = (Equipment)item;
                        if (EquipItem(equipment))
                        {
                            m_playerStats.AddItemStats(equipment);
                            SetSprite(equipment);
                            UpdateStatText();
                            moveFrom.EmptySlot();
                        }
                    }
                }
                //equip item and de equip current item
                else
                {
                    if (moveFrom.item.lvlReq <= m_playerStats.exp.GetCurrentLvl())
                    {
                        Item temp = moveTo.item;
                        Item temp2 = moveFrom.item;

                        moveTo.EmptySlot();
                        moveFrom.EmptySlot();

                        //m_stats.RemoveItemStats(temp);
                        //m_stats.GetItemStats(temp2);

                        RemoveSprite((Equipment)temp);
                        SetSprite((Equipment)temp2);

                        moveTo.SetItem(temp2);
                        moveFrom.SetItem(temp);

                        UpdateStatText();
                    }
                }
            }

            //move to craftingslot
            if (moveFrom.slotType == Slot.SlotType.Inventory && moveTo.slotType == Slot.SlotType.CraftingInput)
            {
                //TODO: check that its correct item somehow (make those inputs have empty stacks or something?)
                if(item != null)
                {
                    if (!moveTo.hasItem)
                    {
                        moveTo.SetItem(item);
                        moveFrom.EmptySlot();
                    }
                    //add stacks
                    else
                    {
                        //if same item and its stackable
                        if (moveTo.item.isStackable && moveTo.item.itemID == moveFrom.item.itemID)
                        {
                            //split stacks
                            int maxStackAmount = moveTo.item.maxStack;
                            int totalAmount = moveTo.item.currentStack + moveFrom.item.currentStack;

                            if (totalAmount > maxStackAmount)
                            {
                                //TODO: make it swap smaller stack with the place of the full stack if we want.. kinda useless
                                moveTo.currentStack = maxStackAmount;
                                moveFrom.currentStack = (totalAmount - maxStackAmount);
                                moveTo.UpdateStackText();
                                moveFrom.UpdateStackText();
                            }
                            else
                            {
                                moveTo.currentStack = totalAmount;
                                moveTo.UpdateStackText();
                                moveFrom.EmptySlot();
                            }
                        }
                    }
                }
            }

            //move from craftingslot to inventory
            if (moveFrom.slotType == Slot.SlotType.CraftingInput && moveTo.slotType == Slot.SlotType.Inventory)
            {
                //TODO: add stacking to existing slot (copy from above)
                if (item != null)
                {
                    if (!moveTo.hasItem)
                    {
                        moveTo.SetItem(item);
                        moveFrom.EmptySlot();
                    }
                    else
                    {
                        //if same item and its stackable
                        if (moveTo.item.isStackable && moveTo.item.itemID == moveFrom.item.itemID)
                        {
                            //split stacks
                            int maxStackAmount = moveTo.item.maxStack;
                            int totalAmount = moveTo.item.currentStack + moveFrom.item.currentStack;

                            if (totalAmount > maxStackAmount)
                            {
                                //TODO: make it swap smaller stack with the place of the full stack if we want.. kinda useless
                                moveTo.currentStack = maxStackAmount;
                                moveFrom.currentStack = (totalAmount - maxStackAmount);
                                moveTo.UpdateStackText();
                                moveFrom.UpdateStackText();
                            }
                            else
                            {
                                moveTo.currentStack = totalAmount;
                                moveTo.UpdateStackText();
                                moveFrom.EmptySlot();
                            }
                        }
                    }
                }
            }

            //movefrom output slot to inventory
            if (moveFrom.slotType == Slot.SlotType.CraftingOutput && moveTo.slotType == Slot.SlotType.Inventory)
            {
                //can only move to free slot
                //TODO: add stacking to existing slot (copy from above)
                if (!moveTo.hasItem)
                {
                    moveTo.SetItem(item);
                    moveFrom.EmptySlot();
                }
                else
                {
                    //if same item and its stackable
                    if (moveTo.item.isStackable && moveTo.item.itemID == moveFrom.item.itemID)
                    {
                        //split stacks
                        int maxStackAmount = moveTo.item.maxStack;
                        int totalAmount = moveTo.item.currentStack + moveFrom.item.currentStack;

                        if (totalAmount > maxStackAmount)
                        {
                            //TODO: make it swap smaller stack with the place of the full stack if we want.. kinda useless
                            moveTo.currentStack = maxStackAmount;
                            moveFrom.currentStack = (totalAmount - maxStackAmount);
                            moveTo.UpdateStackText();
                            moveFrom.UpdateStackText();
                        }
                        else
                        {
                            moveTo.currentStack = totalAmount;
                            moveTo.UpdateStackText();
                            moveFrom.EmptySlot();
                        }
                    }
                }
            }
        }
    }
    public bool EquipItem(Equipment equipment)
    {
        bool returnValue = false;
        if (equipment != null)
        {
            if (equipment.type == Item.Type.equipment)
            {
                foreach (Slot slot in equippedItems)
                {
                    if (slot.equipSlot == equipment.equipSlot && m_playerStats.exp.GetCurrentLvl() >= equipment.lvlReq)
                    {
                        slot.SetItem(equipment);
                        SetSprite(equipment);

                        //hide the slot text
                        slot.transform.GetChild(2).gameObject.SetActive(false);

                        returnValue = true;
                    }
                }
            }
            else
            {
                Debug.Log("Cant equip that item");
            }
        }
        return returnValue;
    }
    public bool IsFull(Item itemToAdd)
    {
        if (itemToAdd != null)
        {
            foreach (Slot slot in inventorySlots)
            {
                if (slot.item != null && slot.item.itemName == itemToAdd.itemName && slot.currentStack < slot.maxStack || !slot.hasItem)
                {
                    return false;
                }
            }
        }
        return true;
    }
    public void UpdateStatText()
    {
        if (m_playerStats != null && statsText != null)
            statsText.text = m_playerStats.GetStatsString();
    }

    [System.Serializable]
    public class InventoryItem
    {
        public int itemId;
        public string itemName;
        public Item item;
        public int slotIndex;
        public bool isStackable;
        public int stackAmount;
        public int stackSize;
    }
}