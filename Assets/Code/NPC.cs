using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Entity, IDropsLoot
{
    public bool dropsLoot = false;

    public int maxItemDropAmount;

    public InteractCollider lootPrefab;

    public int npcType;

    public Faction faction;

    public ReactionToPlayer reactionToPlayer;

    public float agroDistance;

    private Entity m_targetEntity;

    private Rigidbody2D m_rigidbody2D;

    public float moveSpeed;

    public Vector2 facingDirection;
    public float meleeAttackStartRange;
    public float attackRange;
    public float attackWidth;

    private float m_meleeSwingTimer;
    public float meleeSwingTimerMax;

    protected override void Awake()
    {
        base.Awake();
        m_rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public bool IsHostile()
    {
        //TODO: use factions, reputation, etc to figure this out.
        return (reactionToPlayer == ReactionToPlayer.Hostile);
    }

    public bool IsFacingTarget()
    {
        if (m_targetEntity == null)
            FindPlayer();

        if (m_targetEntity == null)
            return false;

        //Vector3 dir = (m_targetEntity.transform.position - transform.position).normalized;
        //TODO: we might want to have entityDirection vector for this?
        //float angle = Vector3.Angle(transform.right, dir);
        float dist = (m_targetEntity.transform.position - transform.position).sqrMagnitude;

        //its faster to multiply than do sqr (especially on mobile)
        return (/*angle < 90 &&*/ dist < agroDistance);
    }

    public void UpdateMeleeTimer()
    {
        if (m_meleeSwingTimer > 0)
            m_meleeSwingTimer -= Time.fixedDeltaTime;
    }

    public void MoveTowards(Vector2 posToMoveTowards)
    {
        if(m_rigidbody2D)
        {
            Vector3 dir = (posToMoveTowards - (Vector2)transform.position).normalized;
            m_rigidbody2D.velocity = dir * Time.deltaTime * moveSpeed;
        }
    }

    public void Stop()
    {
        if (m_rigidbody2D)
        {
            m_rigidbody2D.velocity = Vector2.zero;
        }
    }

    public void Idle()
    {
        if (m_rigidbody2D)
        {
            //TODO: idle around or turn randomly on spot
            m_rigidbody2D.velocity = Vector2.zero;
        }
    }

    public void PerformLightAttack()
    {
        if (m_meleeSwingTimer > 0)
            return;

        m_meleeSwingTimer = meleeSwingTimerMax;

        //TODO: implement angle, cone and range of attack to hit multiple targets
        m_targetEntity.Stats.TakeDamage(Stats.dmg);

        //RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, facingDirection, attackRange);
        /*RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, DirectionToTarget(), attackRange);

        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                Entity hitEntity = hits[i].collider.GetComponent<Entity>();
                //this might not work
                if (hitEntity != null && (hitEntity != this))
                {
                    hitEntity.Stats.TakeDamage(Stats.dmg);
                }
            }
        }*/
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, DirectionToTarget(), Color.red);
    }

    private Vector2 DirectionToTarget()
    {
        if(m_targetEntity)
            return ((Vector2)m_targetEntity.transform.position - (Vector2)transform.position).normalized;
        else
            return Vector2.zero;
    }

    public bool IsCloseEnoughToMelee(Vector2 targetPos)
    {
        float dist = (m_targetEntity.transform.position - transform.position).sqrMagnitude;
        return (dist < meleeAttackStartRange);
    }

    private void FindPlayer()
    {
        m_targetEntity = FindObjectOfType<PlayerEntity>();
    }

    public override void Died()
    {
        Stats.isDead = true;

        //npc died
        //TODO: make a better way to get player reference
        FindObjectOfType<PlayerEntity>().GetComponent<Stats>().AddExp(Stats.GetExpAmount());
        DropLoot();
        Destroy(this.gameObject);
    }

    public void DropLoot()
    {
        if (dropsLoot)
        {
            for (int i = 0; i < maxItemDropAmount; i++)
            {
                if (lootPrefab != null)
                {
                    Item item = ItemManager.lootTables[entityID].GetRandomItem();
                    Debug.Log(entityName + " dropped item " + item.itemName);

                    if (item != null)
                    {
                        InteractCollider lootObject = Instantiate(lootPrefab, transform.position, Quaternion.identity);
                        lootObject.SetLootItem(item);
                    }
                }
            }
        }
    }
}


public enum ReactionToPlayer
{
    Friendly,
    Neutral,
    Unfriendly,
    Hostile
}

public enum Faction
{
    Undead,
    Witches,
    Angels,
    Dragons,
    Commoners,
    Wildlife
}
