using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshPro))]
public class CombatText : MonoBehaviour
{
    private TextMeshPro m_textMesh;
    private Camera m_playerCam;
    private float m_curDuration;
    private float m_maxDuration;
    private Color m_color;
    private Vector3 m_moveVector;
    private float m_originalSize;

    [SerializeField]
    private float m_floatSpeed;
    [SerializeField]
    private float m_critFloatSpeed;
    [SerializeField]
    private float m_fadingSpeed;

    private void Awake()
    {
        m_textMesh = GetComponent<TextMeshPro>();
        m_playerCam = FindObjectOfType<Camera>();
        m_originalSize = m_textMesh.fontSize;
    }

    //TODO: Instead of creating and destroying these -> implement object pooling

    public void Initialize(string value, Color col, float duration, bool isCrit)
    {
        m_textMesh.text = value;
        m_textMesh.color = col;
        m_color = m_textMesh.color;
        m_maxDuration = duration;
        m_curDuration = m_maxDuration;

        //int dir = (Random.Range(0, 2) == 0) ? 1 : -1;

        if (isCrit)
        {
            m_textMesh.fontSize = m_originalSize * 1.5f;
            m_moveVector = new Vector3(Random.Range(-.7f, .7f), 1, 0) * m_critFloatSpeed;
        }
        else
        {
            m_moveVector = new Vector3(Random.Range(-.7f, .7f), 1, 0) * m_floatSpeed;
        }
    }

    private void Update()
    {
        UpdateText();

        if (m_curDuration >= 0)
            m_curDuration -= Time.deltaTime;
        else
            FadeOut();
    }

    public void FadeOut()
    {
        m_color.a -= m_fadingSpeed * Time.deltaTime;
        m_textMesh.color = m_color;
        if (m_color.a < 0)
            Destroy(gameObject);

    }


    public void UpdateText()
    {
        if (m_curDuration > 0)
            transform.position += m_moveVector * Time.deltaTime;

        /*transform.LookAt(transform.position + m_playerCam.transform.rotation * Vector3.forward,
                                       m_playerCam.transform.rotation * Vector3.up);*/

        if (m_curDuration > (m_maxDuration / 2))
        {
            float increaseScale = 2;
            transform.localScale += Vector3.one * increaseScale * Time.deltaTime;
        }
        else if (m_curDuration > 0)
        {
            float increaseScale = 2;
            transform.localScale -= Vector3.one * increaseScale * Time.deltaTime;
        }
    }

}
