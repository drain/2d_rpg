using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Entity))]
public class Stats : MonoBehaviour
{
    public int health = 0;
    public int maxHealth = 0;
    public int healingTaken = 0;
    public int armor = 0;
    public int dmg = 0;
    public bool isDead = false;

    private bool m_isImmune;

    [SerializeField]
    private CombatText m_combatTextObject;

    public void SetImmune(bool value)
    {
        m_isImmune = value;
    }

    [System.Serializable]
    public class Experience
    {
        //public HealthBar hpBar;
        public int currentLvl;
        public int maxLvl;
        public int currentExp;
        public int expToLvl;

        public int totalExpEarned;
        public int expIncrease;

        public int GetCurrentLvl()
        {
            return currentLvl;
        }
        public int GetExpToNextLvl()
        {
            return expToLvl - currentExp;
        }
    }
  
    public Experience exp;

    public void AddStatsByLvl(int lvl)
    {
        health += lvl;
        maxHealth += lvl;
        //armor += lvl;
        dmg += lvl;
    }

    public int GetExpAmount()
    {
        return exp.currentLvl * (int)Mathf.Pow(2, 4);
    }

    public void AddExp(int amount)
    {
        if (isDead)
            return;

        if (exp.currentLvl < exp.maxLvl)
        {
            exp.currentExp += amount;
            exp.totalExpEarned += amount;

            if (exp.currentExp > exp.expToLvl)
            {
                LvlUp();
            }
        }

        if (exp.currentLvl == exp.maxLvl)
            exp.currentExp = 0;

    }

    public void LvlUp()
    {
        int overLvl = exp.currentExp - exp.expToLvl;
        exp.currentExp = overLvl;


        exp.expToLvl = exp.currentLvl + exp.expIncrease * (int)Mathf.Pow(2, (float)exp.currentLvl / 7);

        exp.currentLvl += 1;
        AddStatsByLvl(exp.currentLvl);

        if (exp.currentExp > exp.expToLvl)
        {
            LvlUp();
        }
    }

    public void Heal(float healthIncrease)
    {
        if (health < maxHealth)
        {
            int m_healthIncrease = health + (int)(healthIncrease + (healthIncrease * ((float)healingTaken / 100)));
            if (m_healthIncrease >= maxHealth)
            {
                health = maxHealth;
            }
            else
            {
                health += (int)healthIncrease;
            }
        }

    }

    public void TakeDamage(int damageToTake)
    {
        //TODO: calculate all armor/resistances etc here, also need to pass the type of the damage
        health -= damageToTake;

        if (m_combatTextObject)
        {
            CombatText ct = Instantiate(m_combatTextObject, transform.position, Quaternion.identity);
            ct.Initialize(damageToTake.ToString(), Color.red, .5f, (Random.Range(0, 3) > 0 ? false : true));
        }
        

        if (health <= 0)
        {
            //stats should always have entity object attached
            GetComponent<Entity>().Died();
        }
    }

    public void AddItemStats(Equipment item)
    {
        if (item != null)
        {
            health += item.health;
            maxHealth += item.health;
            armor += item.armor;
            dmg += item.dmg;

            if (item.itemFunction != "")
            {
                switch (item.itemFunction)
                {
                    case "IncreaseHealingTaken":
                        healingTaken += item.parameters[0].getIntValue();
                        break;
                }
            }
        }
    }

    public void RemoveItemStats(Equipment item)
    {
        if (item != null)
        {
            health -= item.health;
            maxHealth -= item.health;
            armor -= item.armor;
            dmg -= item.dmg;

            if (item.itemFunction != "")
            {
                switch (item.itemFunction)
                {
                    case "IncreaseHealingTaken":
                        healingTaken -= item.parameters[0].getIntValue();
                        break;
                }
            }
        }
    }


    public void ResetStats()
    {
        health = 100;
        maxHealth = 100;
        armor = 0;
        //this is needed when our gear matters to our dmg
        //dmg = 0;

        exp.currentExp = 0;
        exp.currentLvl = 1;
        exp.totalExpEarned = 0;
        exp.expToLvl = 82;
        exp.expIncrease = 300;

    }
    public string GetStatsString()
    {
        string returnString = "";
        string tempSplDmgs = "";
        string TemosplResis = "";
        /*foreach (SpellDmg splDmg in spellDmgs)
        {
            tempSplDmgs += splDmg.spellSchool.ToString() + "Dmg% " + splDmg.dmg + "\n";
        }
        foreach (SpellResistance splRes in spellResistances)
        {
            TemosplResis += splRes.spellSchool.ToString() + "Resi% " + splRes.amount + "\n";
        }*/
        returnString += "Stats \n" + "Lvl " + exp.GetCurrentLvl().ToString() + "\n" + "Health " + health + "/" + maxHealth + "\n" + "Armor% " + armor + "\n" +
            "SpellDmg " + dmg + "\n\n" + tempSplDmgs + "\n" + TemosplResis + "\n" + "ExpToNextLvl " + exp.GetExpToNextLvl() +
                "\n" + "TotalExp " + exp.totalExpEarned.ToString();
        return returnString;
    }
}