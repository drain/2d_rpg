using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Equipment", menuName = "ScriptableObjects/Equipment", order = 2)]
public class Equipment : Item
{
    public Inventory.EquipSlot equipSlot;
    public int dmg = 0;
    public int armor = 0;
    public int health = 0;


    public Equipment ()
    {

    }

    public Equipment(Equipment copy)
    {
        DeepCopy(copy);
    }

    public override object Clone()
    {
        //return new Equipment(this);
        return Instantiate(this);
    }

    public void DeepCopy(Equipment item)
    {
        //call base method for copying base item values
        base.DeepCopy(item);

        //copy equipment item values
        equipSlot = item.equipSlot;
        dmg = item.dmg;
        armor = item.armor;
        health = item.health;
    }

    public override string GetTooltip()
    {
        //return base.GetTooltip();
        string returnString = "";
        string splDmgs = "";
        string splResis = "";
        string color = "white";

        switch (rarity)
        {
            case Rarity.common:
                color = "white";
                break;
            case Rarity.uncommon:
                color = "green";
                break;
            case Rarity.rare:
                color = "blue";
                break;
            case Rarity.epic:
                color = "magenta";
                break;
        }

        string tempUseEffect = "";
        if (useEffect != "")
        {
            tempUseEffect += useEffect;
        }

        returnString += "<color=" + color + ">" + itemName + "</Color> \n" + "requires lvl " + lvlReq + "\n" + type.ToString() + "\n" +
                equipSlot.ToString() + "\n" + "BonusHP " + health + "\n" +
                    "Dmg " + dmg.ToString() + "\n" +
                    "Armor " + armor.ToString() + "\n" +
                    splDmgs + "\n" + splResis + "<color=green>" + tempUseEffect + "</color>";
        return returnString;
    }
}
