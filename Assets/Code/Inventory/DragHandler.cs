using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public static Transform moveImage;
    public Transform setMoveImg;
    public static GameObject draggedObject;
    public static Slot draggedSlot;
    public static Item draggedItem;

    public static bool IsDragging;

    private Slot m_slot;
    private Transform m_transform;
    private Vector2 dragStartPos;

    void Awake()
    {
        if (setMoveImg != null)
            moveImage = setMoveImg;
        m_transform = transform;
        m_slot = m_transform.parent.GetComponent<Slot>();
    }
    #region IBeginDragHandler implementation

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if (m_slot.hasItem)
        {
            moveImage.gameObject.SetActive(true);
            moveImage.GetComponent<CanvasGroup>().blocksRaycasts = false;
            moveImage.position = m_transform.position;
        }

        draggedObject = gameObject;
        draggedSlot = m_slot;
        draggedItem = m_slot.item;
        dragStartPos = m_transform.position;
        IsDragging = true;

        if (draggedItem != null)
            moveImage.GetComponent<Image>().sprite = draggedItem.icon;

        m_transform.parent.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    #endregion

    #region IDragHandler implementation

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (m_slot.hasItem)
        {
            m_transform.position = Input.mousePosition;
            moveImage.position = Input.mousePosition;
        }
    }

    #endregion

    #region IEndDragHandler implementation

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        moveImage.gameObject.SetActive(false);
        moveImage.position = dragStartPos;
        moveImage.GetComponent<Image>().sprite = null;
        moveImage.GetComponent<CanvasGroup>().blocksRaycasts = true;

        draggedObject = null;
        draggedItem = null;
        draggedSlot = null;
        IsDragging = false;

        m_transform.position = dragStartPos;
        m_transform.parent.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    #endregion





}