using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEntity : Entity
{
    public Vector2 facingDirection;
    public float attackRange;
    public float attackWidth;

    public void PerformLightAttack()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, facingDirection, attackRange);

        if (hits.Length > 0)
        {
            for(int i = 0; i < hits.Length; i++)
            {
                Entity hitEntity = hits[i].collider.GetComponent<Entity>();
                if (hitEntity != null && hitEntity.tag != "PlayerObject")
                {
                    hitEntity.Stats.TakeDamage(Stats.dmg);
                }
            }
        }

        //check direction
        //get hit angle
        //get distance to enemy
        //check all enemies in scene? or active ones? or
        //wouldnt this loop be super inefficient?



    }

    public override void Died()
    {
        GameManager.Instance.PlayerManager.isEnabled = false;
        Debug.Log("Player died, show dead screen etc.");
    }
}
