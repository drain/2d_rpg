using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public bool isEnabled = true;
    public InputManager inputManager;
    public float moveSpeed;
    public GameObject playerObject;
    public Animator playerAnimator;
    public PlayerUI playerUI;
    public ProfessionManager professionManager;
    public Inventory playerInventory;

    private PlayerEntity m_playerEntity;
    private Text m_playerPromptText;

    private InputSource m_moveLeft;
    private InputSource m_moveRight;
    private InputSource m_moveUp;
    private InputSource m_moveDown;
    private InputSource m_interact;
    private InputSource m_openInventory;
    private InputSource m_openProfessionUI;
    private InputSource m_lightAttack;

    private Vector2 m_inputVector;
    private Vector2 m_animatorVector;
    private Vector3 m_pos, m_velocity;
    private Rigidbody2D m_playerRigidbody2D;

   
    [SerializeField]
    private float m_dashInputTimerMax;
    private float m_dashInputTimer = 0;
    private int m_sameInputInRow;
    private InputSource m_lastInput;

    private float m_dashTimer = 0;
    [SerializeField]
    private float m_dashTimerMax;
    [SerializeField]
    private float m_dashLenght;
    private Vector3 m_lastDashEndPos;

    private Fishing m_fishing;

    private void Awake()
    {
        inputManager = GameManager.Instance.InputManager;
        m_moveLeft = inputManager.GetInputSourceForAction(InputAction.MoveLeft);
        m_moveRight = inputManager.GetInputSourceForAction(InputAction.MoveRight);
        m_moveUp = inputManager.GetInputSourceForAction(InputAction.MoveUp);
        m_moveDown = inputManager.GetInputSourceForAction(InputAction.MoveDown);
        m_interact = inputManager.GetInputSourceForAction(InputAction.Interact);
        m_openInventory = inputManager.GetInputSourceForAction(InputAction.OpenInventory);
        m_openProfessionUI = inputManager.GetInputSourceForAction(InputAction.OpenProfessionUI);
        m_lightAttack = inputManager.GetInputSourceForAction(InputAction.Attack);

        playerObject = GameObject.FindGameObjectWithTag("PlayerObject");
        m_playerRigidbody2D = playerObject.GetComponent<Rigidbody2D>();
        m_playerPromptText = playerObject.GetComponentInChildren<Text>();
        ShowPromptText("", false);

        playerUI = FindObjectOfType<PlayerUI>();
        professionManager = FindObjectOfType<ProfessionManager>();
        playerInventory = GetComponent<Inventory>();
        m_playerEntity = playerObject.GetComponent<PlayerEntity>();

        playerInventory.inventoryUI.SetActive(true);
        professionManager.professionUIMain.SetActive(true);
    }

    private void Start()
    {
        m_pos = playerObject.transform.position;
        //set base stats for lvl 1 character
        playerObject.GetComponent<Stats>().ResetStats();
        playerUI.RefreshPlayerInfoText();

        //TODO: maybe needs null check?
        m_fishing = (Fishing)professionManager.GetProfession(ProfessionType.Fishing);

        playerInventory.inventoryUI.SetActive(false);
        professionManager.professionUIMain.SetActive(false);
    }

    private void Update()
    {
        if(isEnabled)
        {
            MovementInput();
            DashInput();
            CheckInteractions();
            UpdateAnimations();

            if (Input.GetKeyDown(m_openInventory.pc_key))
            {
                if(!DragHandler.IsDragging)
                    playerInventory.inventoryUI.SetActive((playerInventory.inventoryUI.activeSelf) ? false : true);
            }

            if(Input.GetKeyDown(m_openProfessionUI.pc_key))
            {
                if (!DragHandler.IsDragging)
                    professionManager.professionUIMain.SetActive((professionManager.professionUIMain.activeSelf) ? false : true);
            }

            if (Input.GetKeyDown(m_lightAttack.pc_key))
            {
                m_playerEntity.PerformLightAttack();
            }

            //temp remove later
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                playerInventory.inventoryUI.SetActive(false);
                professionManager.professionUIMain.SetActive(false);
            }
        }
    }

    private void FixedUpdate()
    {
        if (isEnabled)
        {
            ApplyMovement(m_inputVector.normalized);

            //m_velocity = (playerObject.transform.position - m_pos) / Time.deltaTime;
            //m_pos = playerObject.transform.position;

            if (m_dashInputTimer > 0)
                m_dashInputTimer -= Time.fixedDeltaTime;

            if (m_dashInputTimer <= 0)
                m_lastInput = null;

            if (m_dashTimer < m_dashTimerMax)
            {
                UpdateDash(m_lastDashEndPos);
                m_dashTimer += Time.fixedDeltaTime;
            }

            m_inputVector = Vector2.zero;
        }
    }

    public void LateUpdate()
    {
        playerUI.RefreshPlayerInfoText();
    }

    public void MovementInput()
    {
        if(Input.GetKey(m_moveLeft.pc_key))
        {
            m_inputVector = Vector2.left;
            m_playerEntity.facingDirection = m_inputVector;
        }
        else if (Input.GetKey(m_moveRight.pc_key))
        {
            m_inputVector = Vector2.right;
            m_playerEntity.facingDirection = m_inputVector;
        }
        else if (Input.GetKey(m_moveUp.pc_key))
        {
            m_inputVector = Vector2.up;
            m_playerEntity.facingDirection = m_inputVector;
        }
        else if (Input.GetKey(m_moveDown.pc_key))
        {
            m_inputVector = Vector2.down;
            m_playerEntity.facingDirection = m_inputVector;
        }  
    }

    private void UpdateAnimations()
    {
        if (playerAnimator != null)
        {
            playerAnimator.SetFloat("speed", m_playerRigidbody2D.velocity.sqrMagnitude);

            if (m_inputVector != Vector2.zero)
                m_animatorVector = m_inputVector;

            playerAnimator.SetFloat("horizontal", m_animatorVector.x);
            playerAnimator.SetFloat("vertical", m_animatorVector.y);
        }
    }

    //TODO: this dash logic is shit make better
    //TODO: Add dash cooldown aswell
    public void DashInput()
    {
        if (Input.GetKeyDown(m_moveLeft.pc_key))
        {
            if(m_lastInput == m_moveLeft && m_dashTimer > 0)
            {
                StartDash();
                m_dashTimer = 0;
                m_lastInput = null;
            }

            m_lastInput = m_moveLeft;
            m_dashInputTimer = m_dashInputTimerMax;

        }
        else if (Input.GetKeyDown(m_moveRight.pc_key))
        {
            if (m_lastInput == m_moveRight && m_dashTimer > 0)
            {
                StartDash();
                m_dashTimer = 0;
                m_lastInput = null;
            }

            m_lastInput = m_moveRight;
            m_dashInputTimer = m_dashInputTimerMax;
        }
        else if (Input.GetKeyDown(m_moveUp.pc_key))
        {
           if (m_lastInput == m_moveUp && m_dashTimer > 0)
            {
                StartDash();
                m_dashTimer = 0;
                m_lastInput = null;
            }

            m_lastInput = m_moveUp;
            m_dashInputTimer = m_dashInputTimerMax;
        }
        else if (Input.GetKeyDown(m_moveDown.pc_key))
        {
            if (m_lastInput == m_moveDown && m_dashTimer > 0)
            {
                StartDash();
                m_dashTimer = 0;
                m_lastInput = null;
            }

            m_lastInput = m_moveDown;
            m_dashInputTimer = m_dashInputTimerMax;
        }
    }

    private void StartDash()
    {
        Vector2 dir = m_playerEntity.facingDirection;
        Vector3 endPos = new Vector3(m_playerEntity.transform.position.x + dir.x * m_dashLenght,
                                     m_playerEntity.transform.position.y + dir.y * m_dashLenght, 0);
        /*Vector3 temp = new Vector3((m_playerEntity.transform.position.x + m_playerEntity.transform.localScale.x) / 2,
                                   (m_playerEntity.transform.position.y + m_playerEntity.transform.localScale.y) / 2) * dir;
        RaycastHit2D hit = Physics2D.Raycast(m_playerEntity.transform.position + temp, endPos);

        if(hit)
        {
            //Vector2 temp = dir * new Vector2(m_playerEntity.transform.position.x * m_playerEntity.transform.localScale.x,
            //                                m_playerEntity.transform.position.y * m_playerEntity.transform.localScale.y) / 2;
            endPos = hit.point;
        }*/

        m_lastDashEndPos = endPos;
    }

    private void OnDrawGizmos()
    {
        if(m_playerEntity != null)
            Debug.DrawLine(m_playerEntity.transform.position, m_lastDashEndPos);
    }

    private void UpdateDash(Vector3 endPos)
    {
        /*if (m_dashTimer >= m_dashTimerMax)
            return;*/

        //m_playerEntity.transform.position = Vector3.Lerp(m_playerEntity.transform.position, endPos, m_dashTimer);
        m_playerRigidbody2D.MovePosition(Vector3.Lerp(m_playerEntity.transform.position, endPos, m_dashTimer));
        //m_playerEntity.transform.position += new Vector3(dir.x * dashLenght, dir.y * dashLenght, 0);
    }

    public void ApplyMovement(Vector2 inputDirection)
    {
        //Vector2 moveVec = inputDirection * Time.deltaTime * moveSpeed;
        //playerObject.transform.Translate(moveVec);
        //m_playerRigidbody2D.MovePosition(inputDirection * Time.deltaTime * moveSpeed);
        m_playerRigidbody2D.velocity = inputDirection * Time.deltaTime * moveSpeed;
    }

    //TODO: fix this while fixing rest of the interactions
    public void CheckInteractions()
    {
        if(m_fishing != null && m_fishing.allowFishing && Input.GetKeyDown(m_interact.pc_key))
        {
            m_fishing.StartFishing();
        }
    }

    public void ShowPromptText(string message, bool show)
    {
        m_playerPromptText.text = message;
        m_playerPromptText.transform.parent.gameObject.SetActive(show);
        m_playerPromptText.gameObject.SetActive(show);
    }
}
