using UnityEngine;
using System;
using System.Collections;
using UnityEditor;

public class MakeScriptableObject : EditorWindow
{
    public ProfessionType professionType;

    [MenuItem("Assets/Create/Profession(SO)", priority = 2)]
    public static void ShowWindow()
    {
        GetWindow(typeof(MakeScriptableObject));
    }

    private void CreateScriptableObject()
    {
        string typeS = professionType.ToString();
        
        //TODO: This will throw error incase class doesnt exist yet, maybe we want that?
        //checking for type doesnt seem to work since this is not during runtime
        ScriptableObject asset = CreateInstance(typeS);
        Profession p = (Profession)asset;
        p.professionType = professionType;
        p.currentSkill = 1;

        string name = AssetDatabase.GenerateUniqueAssetPath($"Assets/Code/Professions/ProfessionObjects/{typeS}.asset");
        AssetDatabase.CreateAsset(asset, name);
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    private void OnGUI()
    {
        GUILayout.Label("Profession to Create:");
        professionType = (ProfessionType)EditorGUILayout.EnumPopup("Profession", professionType);

        if (GUILayout.Button("Create"))
            CreateScriptableObject();
    }
}